#ifndef _INCLUDED_IMAGE_H_
#define _INCLUDED_IMAGE_H_

#include <vector>
#include <string>

// All information about an image.

// To obtain the image's idx use idx()

class Image
{
    typedef std::vector<std::string> StringVector;

    std::string     d_id;               // the id-nr of the image file as text
    StringVector    d_description;      // ("xxxx yyyy") descriptions
                                        // one for each language
    std::vector<StringVector> d_html;   // html info per language
    unsigned        d_idx;              // index of the image

    public:
        Image()
        {}
        Image(std::string const &name, unsigned idx)
        :
            d_id(name),
            d_idx(idx)
        {}
        void addDescription(std::string const &txt)
        {
            d_description.back() += txt;
        }
        void addDescription()
        {
            d_description.push_back("");
        }
        void addHtml(std::string const &line)
        {
            d_html.back().push_back(line);
        }
        void addHtml()
        {
            d_html.push_back(StringVector());
        }
        bool hasId(std::string const &name) const
        {
            return d_id == name;
        }
        std::string const &id() const
        {
            return d_id;
        }
        unsigned const &idx() const
        {
            return d_idx;
        }
        std::string const &description(unsigned idx) const
        {
            return d_description[idx];
        }
        unsigned size()
        {
            return d_description.size();
        }
        StringVector const &html(unsigned language) const
        {
            return d_html[language];
        }
};

#endif
