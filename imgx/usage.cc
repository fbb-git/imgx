#include "main.ih"

void usage()
{
    ArgConfig &arg = ArgConfig::instance();

    cout << 
        arg.basename() << " by Frank B. Brokken (f.b.brokken@rug.nl)\n" 
        "\n" <<
        arg.basename() << " V" << version << "\n"
        "Image Multi Language Multi Category HyperLink Index Generator\n" 
        "Copyright (c) GPL " << years << "\n"
        "Usage:\n" 
        "   " << arg.basename() << " infile outbase\n"
        "Where:\n"
        "   -a [--add-link] url=linktext:   additional hyperlink put just\n"
        "           before the index link. Repeat for additional languages\n"
        "           E.g., -a '/main.html=main table'\n"
        "   -b [--bgcolor] color:           HTML background color "
                                                        " specification:\n"
        "           hexadecimal (including #) or color name\n"
        "   -c [--config-file] pathname: path to a configuration file\n"
        "           (by default ~/.imgx.cf). The configuration may hold "
                                                        "specifications \n"
        "           formatted as 'long-option-name [: value]', where "
                                                    "long-option-name\n"
        "           is the name of a long option without the initial "
                                                    "two hyphens\n"
        "   -B [--background-image] URI:    URI of a background image\n"
        "   -e [--explanation] basename:    add <basename>.<idx> explanation\n"
        "           files to the xxx<idx>.html index page. <idx>: language"
                                                                    "idx\n"
        "   -f [--fgcolor] color:           HTML text (foreground) color "
                                                        " specification:\n"
        "           hexadecimal (including #) or color name\n"
        "   -h [--help]:                    Provide this help\n"
        "   -i [--images-per-file] <nr>:    Max. Number of images per file.\n"
        "           Omit or specify 0 to use one single file per language\n"
        "   -p [--image-prefix] prefix:     Prefix to prepend to the image "
                                                                    "names\n" 
        "           Default prefix: `index'\n"
        "   -v [--version]:                 Display the version and exit\n"
        "   -x [--image-extension] ext:     Extension of the image files\n"
        "           By default: no extension; use a . when required\n"
        "See the distribution's README file for details about the input "
                                                                "format.\n"
        "\n"
        "Input format summary:\n"
        "links:\n"
        "    index   category    chronologically\n"
        "    first previous      next        last   [repeat for each "
                                                                "language]\n"
        "cat:\n"
        "   all cat-names [repeat cat: section for each language]\n"
        "image imgfilename\n"
        "   ( applicable category/ies from 1st cat section )\n"
        "   (\" caption/page text \")   [repeat caption/html for each "
                                                                "language]\n"
        "   <html>                      [html section is optional]\n"
        "       html-text on any number of lines\n"
        "   </html>\n" 
        "\n" 
        "The 'infile' may be split up into several separate files, using\n"
        "#include <nextfile> specifications to switch to 'nextfile';\n"
        "'#include' directives must be followed by at least one blank\n" <<
        endl;

    throw 1;    
}







