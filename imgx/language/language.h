#ifndef _INCLUDED_LANGUAGE_H_
#define _INCLUDED_LANGUAGE_H_

// The Language object returns information about language specific elements.
// It may return the category name given a category index. The category index
// is obtained from an image object. 
//
// The category-less chronological list-name is obtained from all(). 
//
// The members first(), previous(), next() and last() return the language
// translations for the navigation table-columns.
//
// The member head() returns the name of the name of the first `category'
// column in a navigation table, 
//
// The memebr index() returns the name of the `index' table.

#include <string>
#include <vector>
#include <bobcat/exception>

class Language
{
    typedef std::vector<std::string> StringVector;

    StringVector    d_linkStep;
    StringVector    d_catName;
    std::string     d_index;
    std::string     d_head;
    std::string     d_general;
    std::string     d_all;

    public:
        unsigned catSize()
        {
            return d_catName.size();
        }
        unsigned stepSize()
        {
            return d_linkStep.size();
        }
        void setIndex(std::string const &index)
        {
            d_index = index;
        }
        void setHead(std::string const &head)
        {
            d_head = head;
        }
        void setAll(std::string const &all)
        {
            d_all = all;
        }
        void pushStep(std::string const &name)
        {
            d_linkStep.push_back(name);
        }
        void pushCat(std::string const &name)
        {
            d_catName.push_back(name);
        }
        StringVector::const_iterator catBegin() const
        {
            return d_catName.begin();
        }
        StringVector::const_iterator catEnd() const
        {
            return d_catName.end();
        }
        std::string const &catName(unsigned idx) const
        {
            if (idx >= d_catName.size())
                throw FBB::Exception() << "Language::catName " << idx;

            return d_catName[idx];
        }
        std::string const &index() const
        {
            return d_index;
        }
        std::string const &head() const
        {
            return d_head;
        }
        std::string const &all() const
        {
            return d_all;
        }
        std::string const &first() const
        {
            return d_linkStep[0];
        }
        std::string const &prev() const
        {
            return d_linkStep[1];
        }
        std::string const &next() const
        {
            return d_linkStep[2];
        }
        std::string const &last() const
        {
            return d_linkStep[3];
        }
};

#endif
