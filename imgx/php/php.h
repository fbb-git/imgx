#ifndef _INCLUDED_PHP_
#define _INCLUDED_PHP_

#include <iostream>

#include "../writebase/writebase.h"
#include "../parser/parser.h"

class Php
{
    WriteBase &d_wb;

    public:
        Php(WriteBase &wb);

        void catLinkArray(unsigned idx);    // 0: all catlink arrays of 
                                            // the images in one php file
    private:
        void catArray(unsigned idx);    // 1: catlink elements for image `idx'
        void imageCats(unsigned idx);   // 2: category links for image `idx'
        void allCatLink(unsigned idx);   // 3: the `all images' linkarray
        void activeCatLinks(unsigned idx);   // 3

        void catLink();                 // 1: inserts  the php catlink
                                        //    function. 


                                        // array() definition for image `next'
        void array(unsigned next, unsigned current, unsigned hyperlink);    
        std::string fileIdx(unsigned idx) const;    // language + file index
                                                    // given imageindex
};

        
#endif
