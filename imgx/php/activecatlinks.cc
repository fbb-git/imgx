#include "php.ih"

void Php::activeCatLinks(unsigned idx)
{
    for (unsigned qidx = 0; qidx < d_wb.nQueues(); ++qidx)
    {
        Queue const &qu = d_wb.queue(qidx);

        if (!qu.find(idx))           // this idx is not in the cat. queue
            continue;

                                        // get the name of the category
        d_wb.out() << "          '" << d_wb.lang().catName(qu.catIdx()) << 
                                                        "' => array(\n"
             "              ";

        array(qu.firstImageIndex(), idx, 1); // first image, this cat.

        if (qu.hasPrevious())
            array(qu.previousImageIndex(), idx,
                                qu.previousNr()); // previous image idx
        else
            d_wb.out() << "'-', ";                           // no previous idx
            
        if (qu.hasNext())
            array(qu.nextImageIndex(), idx,
                                qu.nextNr());        // next image idx
        else
            d_wb.out() << "'-', ";                           // no next idx
            
        array(qu.lastImageIndex(), idx,
                                qu.lastNr());        // last image
        d_wb.out() << "\n"
                "           ),\n";
    }
}


