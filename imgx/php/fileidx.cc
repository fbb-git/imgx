#include "php.ih"

string Php::fileIdx(unsigned imageIdx) const
{
    ostringstream out;
    out << d_wb.langIdx() << (imageIdx / d_wb.imagesPerFile());
    return out.str();
}
