#include "php.ih"

void Php::catLink()       // insert the php catLink() function
{
    d_wb.out() <<
    "function catLink($idx)\n"
    "{    \n"
    "    global $cat, $hash;\n"
    "    $catInfo =& $hash[$idx];               // cat info of image $idx\n"
    "\n"
    "    if (!array_key_exists($cat, $catInfo)) // no category, no "
                                                            "quicklink\n"
    "        return;\n"
    "\n"
    "     $arr =& $catInfo[$cat];   // the array info for this "
                                                        "image/cat combi\n"
    "\n"
    "    echo <<< NavTableHead\n";

    d_wb.startNavTable(WriteBase::IMAGEFILE);

    d_wb.out() <<                    // fill in the links
    "    <tr>\n"
    "      <td align=\"right\"> $cat &nbsp;&nbsp;</td>\n"
    "NavTableHead;\n"
    "    for ($el = 0; $el < 4; ++$el)\n"
    "    {\n"
    "        echo    '<td align=\"center\">';\n"
    "        if (is_array($arr[$el]))\n"
    "            echo    '<a href=\"" << d_wb.outbase() << 
                                "', $arr[$el][0], '.php?cat=', $cat, \n"
    "                   '#i', $arr[$el][1], '\">', $arr[$el][2], '</a>';\n"
    "        else\n"
    "            echo $arr[$el];\n"
    "        echo    \"</td>\\n\";\n"
    "    }\n"
    "    echo <<< NavTableEnd\n"
    "    </tr>\n";

    d_wb.endNavTable();

    d_wb.out() <<
    "NavTableEnd;\n"
    "}\n";
}


