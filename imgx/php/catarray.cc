#include "php.ih"

void Php::catArray(unsigned idx)  // image index
{
    d_wb.out() << 
    "       " << idx << " => array(   // category info this index\n";

    imageCats(idx);                     // all category associative arrays

    d_wb.out() <<
    "       ),\n";
}

