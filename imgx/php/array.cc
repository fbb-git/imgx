#include "php.ih"

void Php::array(unsigned next, unsigned current, unsigned hyperlink)
{
    if (next == static_cast<unsigned>(~0) || next >= d_wb.nImages())
        d_wb.out() << "'-', ";
    else if (next == current)
        d_wb.out() << "'" << hyperlink << "', ";
    else
        d_wb.out() << "array('" << fileIdx(next) << "', '" << next << 
                            "', " << hyperlink << "), ";
}

