#include "php.ih"

void Php::allCatLink(unsigned idx)    // image index
{
    d_wb.out() << "          '" << d_wb.lang().all() << "' => array(\n"
             "              ";
    array(0, idx, 1);                // first image
    array(idx - 1, idx, idx);        // previous image (doesn't exist)
    array(idx + 1, idx, idx + 2);    // next image (if available)
    array(d_wb.nImages() - 1, idx, d_wb.nImages());  // last image
    d_wb.out() << "\n"
            "           ),\n";
}

