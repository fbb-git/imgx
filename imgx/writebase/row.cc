#include "writebase.ih"

void WriteBase::row(std::string c1, std::string c2, std::string c3,
                 std::string c4, std::string c5)
{
    d_out << "<tr>\n";
    d_out << "  <td align=\"right\">" << c1 << "&nbsp;&nbsp;</td>\n";
    col(c2);
    col(c3);
    col(c4);
    col(c5);
    d_out << "</tr>\n";
}
