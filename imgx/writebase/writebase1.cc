#include "writebase.ih"

WriteBase::WriteBase(Parser const &parser)
:
    d_parser(parser),
    d_langIdx(0)
{
    ArgConfig &arg = ArgConfig::instance();

    d_outbase = arg[1];

    string tmp;
    if (not arg.option(&tmp, 'i') || (d_imagesPerFile = stoul(tmp)) == 0)
        d_imagesPerFile = ~0;

    arg.option(&d_imgext, 'x');

    arg.option(&d_imgprefix, 'p');
}
