#ifndef _INCLUDED_WRITEBASE_
#define _INCLUDED_WRITEBASE_

#include <fstream>
#include <vector>

#include "../queue/queue.h"
#include "../image/image.h"
#include "../language/language.h"
#include "../parser/parser.h"

class WriteBase
{
    std::ofstream d_out;
    Parser const &d_parser;
    unsigned d_langIdx;
    std::string d_outbase;
    std::string d_imgprefix;
    std::string d_imgext;
    unsigned d_imagesPerFile;

    public:
        enum NavTabType
        {
            INDEXFILE,
            IMAGEFILE,
        };

        WriteBase(Parser const &parser);

        void setLangIdx(unsigned idx);
        void incLangIdx();
        unsigned langIdx() const;
        unsigned imagesPerFile() const;
        std::string const &outbase() const;
        std::string const &imgPrefix() const;
        std::string const &imgExt() const;
        void rowline();
        void col(std::string const &str);
        void row(std::string c1, std::string c2, std::string c3,
                 std::string c4, std::string c5);
        void endNavTable();
        void startNavTable(NavTabType type);
        Language const &lang() const;

        std::vector<Language> const &languages() const;

        std::vector<Image> const &images() const;

        std::vector<Queue> const &queues() const;
        std::ofstream &out();
        unsigned nImages() const;
        Image const &image(unsigned idx) const;
        unsigned nQueues() const;
        Queue const &queue(unsigned idx) const;
};

inline void WriteBase::setLangIdx(unsigned idx)
{
    d_langIdx = idx;
}
inline void WriteBase::incLangIdx()
{
    ++d_langIdx;
}
inline unsigned WriteBase::langIdx() const
{
    return d_langIdx;
}
inline unsigned WriteBase::imagesPerFile() const
{
    return d_imagesPerFile;
}
inline std::string const &WriteBase::outbase() const
{
    return d_outbase;
}
inline std::string const &WriteBase::imgPrefix() const
{
    return d_imgprefix;
}
inline std::string const &WriteBase::imgExt() const
{
    return d_imgext;
}
inline Language const &WriteBase::lang() const
{
    return d_parser.lang(d_langIdx);
}

inline std::vector<Language> const &WriteBase::languages() const
{
    return d_parser.languages();
}

inline std::vector<Image> const &WriteBase::images() const
{
    return d_parser.images();
}

inline std::vector<Queue> const &WriteBase::queues() const
{
    return d_parser.queues();
}
inline std::ofstream &WriteBase::out()
{
    return d_out;
}        
inline unsigned WriteBase::nImages() const
{
    return d_parser.nImages();
}
inline Image const &WriteBase::image(unsigned idx) const
{
    return d_parser.image(idx);
}
inline unsigned WriteBase::nQueues() const
{
    return d_parser.nQueues();
}
inline Queue const &WriteBase::queue(unsigned idx) const
{
    return d_parser.queue(idx);
}

#endif




