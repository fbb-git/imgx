#include "writebase.ih"

void WriteBase::startNavTable(NavTabType type)
{
    d_out << "<table> <!-- navigation table -->\n";
    
    rowline();                                  // the navigation labels

    if (type == IMAGEFILE)
        row(lang().head(),  lang().first(), lang().prev(), lang().next(),  
                                                           lang().last());
    else
        row(lang().head(), lang().first(), lang().last(), "", "");

    rowline();                                  
}

