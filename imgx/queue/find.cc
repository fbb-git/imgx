#include "queue.ih"

bool Queue::find(unsigned idx) const
{
    Queue::const_iterator it = std::find(begin(), end(), idx);
    if (it == end())
        return false;

    d_idx = it - begin();
    return true;
}
