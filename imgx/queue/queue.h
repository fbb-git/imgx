#ifndef _INCLUDED_QUEUE_H_
#define _INCLUDED_QUEUE_H_

#include <vector>

#include <iostream>

// A Queue contains information about a certain image index: its 
// category index, its current, previous, next and last image index, and
// members to tell you whether that index exists (e.g., hasCurrent() and
// currentIndex() to get that index.

class Queue: public std::vector<unsigned>
{
    typedef std::vector<std::string> StringVector;

    mutable unsigned d_idx;     // mutable by find()
    unsigned d_catIdx;          // category idx in d_language in the parser
                                // for which this queue holds

    public:
        void setCatIdx(unsigned catIdx)
        {
            d_catIdx = catIdx;
        }
        unsigned catIdx() const
        {
            return d_catIdx;
        }
        bool find(unsigned idx) const;
            
        unsigned currentImageIndex() const
        {
            return (*this)[d_idx];
        }
        unsigned currentIndex() const
        {
            return d_idx;
        }
        unsigned currentNr() const
        {
            return d_idx + 1;
        }

        unsigned firstImageIndex() const
        {
            return front();
        }

        bool hasPrevious() const
        {
            return d_idx > 0;
        }
        unsigned previousImageIndex() const
        {
            return (*this)[d_idx - 1];
        }
        unsigned previousIndex() const
        {
            return d_idx - 1;
        }
        unsigned previousNr() const
        {
            return d_idx;
        }

        bool hasNext() const
        {
            return d_idx + 1 < size();
        }
        unsigned nextImageIndex() const
        {
            return (*this)[d_idx + 1];
        }
        unsigned nextIndex() const
        {
            return d_idx + 1;
        }
        unsigned nextNr() const
        {
            return d_idx + 2;
        }

        bool atLast() const
        {
            return d_idx + 1 == size();
        }
        unsigned lastImageIndex() const
        {
            return back();
        }
        unsigned lastIndex() const
        {
            return size() - 1;
        }
        unsigned lastNr() const
        {
            return size();
        }

};

#endif
