/*
                              imgx.cc
*/

#include "main.ih"

namespace{
    Arg::LongOption longOptions[] = 
    {
        Arg::LongOption("add-link",         'a'),
        Arg::LongOption("background-color", 'b'),
        Arg::LongOption("background-image", 'B'),
        Arg::LongOption("config-file",      'c'),
        Arg::LongOption("explanation",      'e'),
        Arg::LongOption("foreground-color", 'f'),
        Arg::LongOption("help",             'h'),
        Arg::LongOption("images-per-file",  'i'),
        Arg::LongOption("image-prefix",     'p'),
//        Arg::LongOption("separate-index",   's'),
        Arg::LongOption("version",          'v'),
        Arg::LongOption("image-extension",  'x'),
    };
    Arg::LongOption *const endOpt = longOptions + sizeof(longOptions) /
                                                    sizeof(Arg::LongOption);
}

int main(int argc, char **argv)
try
{
    ArgConfig &arg = 
        ArgConfig::initialize("a:b:B:c:e:f:hi:p:vx:", longOptions, endOpt, 
                                argc, argv,
                                ArgConfig::RemoveComment);

    if (arg.option('v') && !arg.option('h'))
    {
        cout << arg.basename() << " version " << version << 
                " (Frank B. Brokken, f.b.brokken@rug.nl, " <<
                years << ")" << endl;
        return 0;
    }

    if (arg.nArgs() < 2 || arg.option('h') )
        usage();

    string configName;
    if (not arg.option(&configName, 'c'))
        configName = User().homedir() + ".imgx.cf";

    if (Stat(configName))
        arg.open(configName);

    Parser parser;

    if (!parser.parse() && !parser.errors())
    {
        Generator generator(parser);

        generator.generate();
    }
}
catch(std::exception const &e)
{
    cout << e.what() << '\n';
    return 1;
}
catch(int x)
{
    return 1;
}

