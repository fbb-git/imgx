// $insert class.ih
#include "parser.ih"


namespace // anonymous
{
    enum ReservedTokens
    {
       PARSE_ACCEPT     = 0,   // `ACCEPT' TRANSITION
       _UNDETERMINED_   = -2,
       _EOF_            = -1,
       _error_          = 256,
    };
    enum StateType       // modify data.cc when this enum changes
    {
        NORMAL,
        HAS_ERROR_ITEM,
        IS_ERROR_STATE,
    };    
    struct PI   // Production Info
    {
        unsigned d_nonTerm; // identification number of this production's
                            // non-terminal 
        unsigned d_size;    // number of elements in this production 
    };

    struct SR   // Shift Reduce info
    {
        union
        {
            int _field_1_;      // initializer, allowing initializations 
                                // of the SR s_[] arrays
            StateType   d_type;
            unsigned    d_symbol;  
            int         d_token;
        };
        union
        {
            int _field_2_;

            int d_lastIdx;          // if negative, the state uses SHIFT
            int d_negProductionNr;  // if positive, use d_nextState
            unsigned d_nextState;   
                            // negative: negative rule number to reduce to
                            // otherwise: next state
            unsigned d_errorState;  // used with Error states
        };
        // identification number (value) of a terminal
        // (action table) or non-terminal (goto table). In
        // SR arrays d_symbol of the first SR element defines the state's
        // type, while d_action holds the last element's index. The field
        // d_symbol of the last element of an SR array will contain the
        // last retrieved token and is used to speed up the seach. If the
        // last element's d_action field is non-negative, it is the state
        // to go to when an error is encountered. In that case, the stack
        // is reduced to this state (so, to 0 at the stacktop with with
        // s_0, etc.) and the indicated state is pushed on the stack,
        // representing shifting `error'. Then, the error routine will
        // continue to retrieve tokens from the input until the next
        // transition is possible from that state, and thus error recovery
        // has been performed
        // So:
        //      s_x[] = 
        //      {
        //          {state-type,        idx of last element},
        //          {symbol,            < 0: reduce, >= 0: next state},
        //          ...
        //          {set to last retrieved token, 
        //                              next state if >= 0 on error},
        //      }
    };

    // $insert staticdata
    
// Productions Info Records:
PI s_productionInfo[] = 
{
     {0, 0}, // not used: reduction values are negative
     {267, 3}, //     1: startrule -> links optcats images
     {271, 1}, //     2: allname -> IDENT
     {272, 2}, //     3: cat -> catdef catlist
     {273, 2}, //     4: catdef -> CAT ':'
     {275, 1}, //     5: category -> IDENT
     {274, 2}, //     6: catlist -> catlist catname
     {274, 1}, //     7: catlist -> catname
     {276, 1}, //     8: catname -> IDENT
     {269, 1}, //     9: optcats -> cats
     {269, 0}, //     10: optcats ->    <empty>
     {277, 2}, //     11: cats -> cats cat
     {277, 1}, //     12: cats -> cat
     {278, 2}, //     13: optcatset -> optcatset IDENT
     {278, 0}, //     14: optcatset ->    <empty>
     {279, 1}, //     15: descriptions -> imagedescriptions
     {281, 3}, //     16: imagecats -> '(' optcatset ')'
     {282, 1}, //     17: line -> LINE
     {283, 0}, //     18: lines ->    <empty>
     {283, 2}, //     19: lines -> lines line
     {284, 0}, //     20: opthtmltext ->    <empty>
     {284, 2}, //     21: opthtmltext -> HTML lines
     {285, 5}, //     22: imagedescription -> '(' newdescription string ')' opthtmltext
     {280, 2}, //     23: imagedescriptions -> imagedescriptions imagedescription
     {280, 1}, //     24: imagedescriptions -> imagedescription
     {288, 1}, //     25: imageid -> IDENT
     {270, 2}, //     26: images -> images image
     {270, 1}, //     27: images -> image
     {289, 4}, //     28: image -> IMAGE imageid imagecats descriptions
     {289, 2}, //     29: image -> IMAGE error
     {290, 2}, //     30: index -> IDENT newlanguage
     {268, 3}, //     31: links -> LINKS ':' withinlinks
     {286, 0}, //     32: newdescription ->    <empty>
     {291, 0}, //     33: newlanguage ->    <empty>
     {293, 1}, //     34: step -> IDENT
     {294, 7}, //     35: steps -> index category allname step step step step
     {294, 1}, //     36: steps -> error
     {287, 2}, //     37: string -> string onestring
     {287, 1}, //     38: string -> onestring
     {295, 1}, //     39: onestring -> STRING
     {292, 2}, //     40: withinlinks -> withinlinks steps
     {292, 1}, //     41: withinlinks -> steps
     {296, 1}, //     42: startrule_$ -> startrule
};

// State info and SR transitions for each state.


SR s_0[] =
{
    {NORMAL, 4}, // SHIFTS
    {ParserBase::LINKS, 1},
    {267, 2}, // startrule
    {268, 3}, // links
    {0, 0}
};

SR s_1[] =
{
    {NORMAL, 2}, // SHIFTS
    {':', 4},
    {0, 0}
};

SR s_2[] =
{
    {NORMAL, -2},
    {_EOF_, PARSE_ACCEPT},
    {0, 0}
};

SR s_3[] =
{
    {NORMAL, 6}, // SHIFTS
    {ParserBase::CAT, 5},
    {269, 6}, // optcats
    {272, 7}, // cat
    {273, 8}, // catdef
    {277, 9}, // cats
    {0, -10} // DEFAULT_REDUCTION
};

SR s_4[] =
{
    {HAS_ERROR_ITEM, 6}, // SHIFTS
    {_error_, 10},
    {ParserBase::IDENT, 11},
    {290, 12}, // index
    {292, 13}, // withinlinks
    {294, 14}, // steps
    {0, 0}
};

SR s_5[] =
{
    {NORMAL, 2}, // SHIFTS
    {':', 15},
    {0, 0}
};

SR s_6[] =
{
    {NORMAL, 4}, // SHIFTS
    {ParserBase::IMAGE, 16},
    {270, 17}, // images
    {289, 18}, // image
    {0, 0}
};

SR s_7[] =
{
    {NORMAL, -1},
    {0, -12} // DEFAULT_REDUCTION
};

SR s_8[] =
{
    {NORMAL, 4}, // SHIFTS
    {ParserBase::IDENT, 19},
    {274, 20}, // catlist
    {276, 21}, // catname
    {0, 0}
};

SR s_9[] =
{
    {NORMAL, 4}, // SHIFTS
    {ParserBase::CAT, 5},
    {272, 22}, // cat
    {273, 8}, // catdef
    {0, -9} // DEFAULT_REDUCTION
};

SR s_10[] =
{
    {IS_ERROR_STATE, -1},
    {0, -36} // DEFAULT_REDUCTION
};

SR s_11[] =
{
    {NORMAL, -2},
    {291, 23}, // newlanguage
    {0, -33} // DEFAULT_REDUCTION
};

SR s_12[] =
{
    {NORMAL, 3}, // SHIFTS
    {ParserBase::IDENT, 24},
    {275, 25}, // category
    {0, 0}
};

SR s_13[] =
{
    {HAS_ERROR_ITEM, 5}, // SHIFTS
    {_error_, 10},
    {ParserBase::IDENT, 11},
    {290, 12}, // index
    {294, 26}, // steps
    {0, -31} // DEFAULT_REDUCTION
};

SR s_14[] =
{
    {NORMAL, -1},
    {0, -41} // DEFAULT_REDUCTION
};

SR s_15[] =
{
    {NORMAL, -1},
    {0, -4} // DEFAULT_REDUCTION
};

SR s_16[] =
{
    {HAS_ERROR_ITEM, 4}, // SHIFTS
    {_error_, 27},
    {ParserBase::IDENT, 28},
    {288, 29}, // imageid
    {0, 0}
};

SR s_17[] =
{
    {NORMAL, 3}, // SHIFTS
    {ParserBase::IMAGE, 16},
    {289, 30}, // image
    {0, -1} // DEFAULT_REDUCTION
};

SR s_18[] =
{
    {NORMAL, -1},
    {0, -27} // DEFAULT_REDUCTION
};

SR s_19[] =
{
    {NORMAL, -1},
    {0, -8} // DEFAULT_REDUCTION
};

SR s_20[] =
{
    {NORMAL, 3}, // SHIFTS
    {ParserBase::IDENT, 19},
    {276, 31}, // catname
    {0, -3} // DEFAULT_REDUCTION
};

SR s_21[] =
{
    {NORMAL, -1},
    {0, -7} // DEFAULT_REDUCTION
};

SR s_22[] =
{
    {NORMAL, -1},
    {0, -11} // DEFAULT_REDUCTION
};

SR s_23[] =
{
    {NORMAL, -1},
    {0, -30} // DEFAULT_REDUCTION
};

SR s_24[] =
{
    {NORMAL, -1},
    {0, -5} // DEFAULT_REDUCTION
};

SR s_25[] =
{
    {NORMAL, 3}, // SHIFTS
    {ParserBase::IDENT, 32},
    {271, 33}, // allname
    {0, 0}
};

SR s_26[] =
{
    {NORMAL, -1},
    {0, -40} // DEFAULT_REDUCTION
};

SR s_27[] =
{
    {IS_ERROR_STATE, -1},
    {0, -29} // DEFAULT_REDUCTION
};

SR s_28[] =
{
    {NORMAL, -1},
    {0, -25} // DEFAULT_REDUCTION
};

SR s_29[] =
{
    {NORMAL, 3}, // SHIFTS
    {'(', 35},
    {281, 34}, // imagecats
    {0, 0}
};

SR s_30[] =
{
    {NORMAL, -1},
    {0, -26} // DEFAULT_REDUCTION
};

SR s_31[] =
{
    {NORMAL, -1},
    {0, -6} // DEFAULT_REDUCTION
};

SR s_32[] =
{
    {NORMAL, -1},
    {0, -2} // DEFAULT_REDUCTION
};

SR s_33[] =
{
    {NORMAL, 3}, // SHIFTS
    {ParserBase::IDENT, 36},
    {293, 37}, // step
    {0, 0}
};

SR s_34[] =
{
    {NORMAL, 5}, // SHIFTS
    {'(', 40},
    {279, 38}, // descriptions
    {280, 39}, // imagedescriptions
    {285, 41}, // imagedescription
    {0, 0}
};

SR s_35[] =
{
    {NORMAL, -2},
    {278, 42}, // optcatset
    {0, -14} // DEFAULT_REDUCTION
};

SR s_36[] =
{
    {NORMAL, -1},
    {0, -34} // DEFAULT_REDUCTION
};

SR s_37[] =
{
    {NORMAL, 3}, // SHIFTS
    {ParserBase::IDENT, 36},
    {293, 43}, // step
    {0, 0}
};

SR s_38[] =
{
    {NORMAL, -1},
    {0, -28} // DEFAULT_REDUCTION
};

SR s_39[] =
{
    {NORMAL, 3}, // SHIFTS
    {'(', 40},
    {285, 44}, // imagedescription
    {0, -15} // DEFAULT_REDUCTION
};

SR s_40[] =
{
    {NORMAL, -2},
    {286, 45}, // newdescription
    {0, -32} // DEFAULT_REDUCTION
};

SR s_41[] =
{
    {NORMAL, -1},
    {0, -24} // DEFAULT_REDUCTION
};

SR s_42[] =
{
    {NORMAL, 3}, // SHIFTS
    {ParserBase::IDENT, 46},
    {')', 47},
    {0, 0}
};

SR s_43[] =
{
    {NORMAL, 3}, // SHIFTS
    {ParserBase::IDENT, 36},
    {293, 48}, // step
    {0, 0}
};

SR s_44[] =
{
    {NORMAL, -1},
    {0, -23} // DEFAULT_REDUCTION
};

SR s_45[] =
{
    {NORMAL, 4}, // SHIFTS
    {ParserBase::STRING, 49},
    {287, 50}, // string
    {295, 51}, // onestring
    {0, 0}
};

SR s_46[] =
{
    {NORMAL, -1},
    {0, -13} // DEFAULT_REDUCTION
};

SR s_47[] =
{
    {NORMAL, -1},
    {0, -16} // DEFAULT_REDUCTION
};

SR s_48[] =
{
    {NORMAL, 3}, // SHIFTS
    {ParserBase::IDENT, 36},
    {293, 52}, // step
    {0, 0}
};

SR s_49[] =
{
    {NORMAL, -1},
    {0, -39} // DEFAULT_REDUCTION
};

SR s_50[] =
{
    {NORMAL, 4}, // SHIFTS
    {ParserBase::STRING, 49},
    {')', 53},
    {295, 54}, // onestring
    {0, 0}
};

SR s_51[] =
{
    {NORMAL, -1},
    {0, -38} // DEFAULT_REDUCTION
};

SR s_52[] =
{
    {NORMAL, -1},
    {0, -35} // DEFAULT_REDUCTION
};

SR s_53[] =
{
    {NORMAL, 3}, // SHIFTS
    {ParserBase::HTML, 55},
    {284, 56}, // opthtmltext
    {0, -20} // DEFAULT_REDUCTION
};

SR s_54[] =
{
    {NORMAL, -1},
    {0, -37} // DEFAULT_REDUCTION
};

SR s_55[] =
{
    {NORMAL, -2},
    {283, 57}, // lines
    {0, -18} // DEFAULT_REDUCTION
};

SR s_56[] =
{
    {NORMAL, -1},
    {0, -22} // DEFAULT_REDUCTION
};

SR s_57[] =
{
    {NORMAL, 3}, // SHIFTS
    {ParserBase::LINE, 58},
    {282, 59}, // line
    {0, -21} // DEFAULT_REDUCTION
};

SR s_58[] =
{
    {NORMAL, -1},
    {0, -17} // DEFAULT_REDUCTION
};

SR s_59[] =
{
    {NORMAL, -1},
    {0, -19} // DEFAULT_REDUCTION
};


// State array:
SR *s_state[] =
{
  s_0,  s_1,  s_2,  s_3,  s_4,  s_5,  s_6,  s_7,  s_8,  s_9,
  s_10,  s_11,  s_12,  s_13,  s_14,  s_15,  s_16,  s_17,  s_18,  s_19,
  s_20,  s_21,  s_22,  s_23,  s_24,  s_25,  s_26,  s_27,  s_28,  s_29,
  s_30,  s_31,  s_32,  s_33,  s_34,  s_35,  s_36,  s_37,  s_38,  s_39,
  s_40,  s_41,  s_42,  s_43,  s_44,  s_45,  s_46,  s_47,  s_48,  s_49,
  s_50,  s_51,  s_52,  s_53,  s_54,  s_55,  s_56,  s_57,  s_58,  s_59,
};


} // anonymous namespace ends


// If the parser call uses arguments, then provide an overloaded function.
// The code below doesn't rely on parameters, so no arguments are required.
// Furthermore, parse uses a function try block to allow us to do
// ACCEPT and ABORT from anywhere, even from within members called
// by actions, simply throwing the appropriate exceptions.

ParserBase::ParserBase()
:
    d_stackIdx(-1),
    // $insert debuginit
    d_debug(false),
    d_nErrors(0),
    d_token(_UNDETERMINED_)
{}

void ParserBase::ABORT() const throw(Return) 
{
    throw PARSE_ABORT;
}

void ParserBase::ACCEPT() const throw(Return)
{
    throw PARSE_ACCEPT;
}

void ParserBase::ERROR() const throw(ErrorRecovery)
{
    throw UNEXPECTED_TOKEN;
}

void ParserBase::clearin()
{
    d_token = _UNDETERMINED_;
}


void ParserBase::push(unsigned state)
{
    if (static_cast<unsigned>(d_stackIdx + 1) == d_stateStack.size())
    {
        unsigned newSize = d_stackIdx + 5;
        d_stateStack.resize(newSize);
        d_valueStack.resize(newSize);
    }
    ++d_stackIdx;
    d_stateStack[d_stackIdx] = d_state = state;
    *(d_vsp = &d_valueStack[d_stackIdx]) = d_val;
}

void ParserBase::pop(unsigned count)
{
    d_stackIdx -= count;
    d_state = d_stateStack[d_stackIdx];
    d_vsp = &d_valueStack[d_stackIdx];
}

unsigned ParserBase::top() const
{
    if (d_stackIdx < 0)
    {
        throw DEFAULT_RECOVERY_MODE;
    }

    return d_stateStack[d_stackIdx];
}

unsigned ParserBase::reduce(PI const &pi)
{

    pop(pi.d_size);


    return pi.d_nonTerm;
}

void Parser::executeAction(int production)
{
    switch (production)
    {
        // $insert actioncases
        
        case 2:
        {
                d_language[d_langidx].setAll(d_scanner.YYText());
            }
        break;

        case 3:
        {
                checkCatListSize();
            }
        break;

        case 5:
        {
                d_language[d_langidx].setHead(d_scanner.YYText());
            }
        break;

        case 8:
        {
                pushCatName(d_scanner.YYText());
            }
        break;

        case 9:
        {
                checkCatCount();
            }
        break;

        case 13:
        {
                addCategory(d_scanner.YYText());
            }
        break;

        case 15:
        {
                checkDescCount();
            }
        break;

        case 17:
        {
                addHtml(d_scanner.YYText());   
            }
        break;

        case 25:
        {
                addImage(d_scanner.YYText());
            }
        break;

        case 30:
        {
                d_language[d_langidx].setIndex(d_scanner.YYText());
            }
        break;

        case 31:
        {
                checkLastStepSize();
            }
        break;

        case 32:
        {
                d_image.back().addDescription();
                d_image.back().addHtml();
            }
        break;

        case 33:
        {
                d_language.push_back(Language());
            }
        break;

        case 34:
        {
                d_language.back().pushStep(d_scanner.YYText());
            }
        break;

        case 35:
        {
                ++d_langidx;
            }
        break;

        case 39:
        {
                d_image.back().addDescription(d_scanner.str());
            }
        break;

    }
}

int Parser::nextToken()
{
    d_token = lex();
    print();
    if (d_token <= 0)
        d_token = _EOF_;

    return d_token;
}
        
int Parser::lookup(int token)
{
    SR *sr = s_state[d_state];

    int lastIdx = sr->d_lastIdx;

    if (lastIdx < 0)                    // doesn't shift
        lastIdx = -lastIdx;
    else if (token == _UNDETERMINED_)   // shift if token isn't available
        token = nextToken();            // (e.g., following a reduce it *is*
                                        // available)
    SR *last = sr + lastIdx;

    last->d_token = token;              // set search-token
    
    SR *search = sr + 1;
    while (search->d_token != token)
        ++search;

    if (search == last && !last->d_negProductionNr)
    {
        throw UNEXPECTED_TOKEN;
    }

    return search->d_negProductionNr;
}

    // When an error has occurred, pop elements off the stack until the top
    // state has an error-transition. If none is found, the default recovery
    // mode (which is to abort) is activated. 
    //
    // If an error state is found, then that state's last SR element holds at
    // its d_errorState field the state to go to on `error'.  That state is
    // pushed on the state-stack, to become the next state. 
    //
    // In that state, the token (causing the error) is skipped and subsequent
    // tokens (retrieved from the input) are looked up. If a match is found,
    // then from this point parsing continues. 
    //
    // If EOF is encountered without being appropriate for the current state,
    // then the error recovery will fall back to the default recovery mode.
    // (i.e., parsing terminates)
unsigned Parser::errorRecovery()
try
{
    ++d_nErrors;

    error("Syntax error");

    while (s_state[top()][0].d_type != HAS_ERROR_ITEM)
        pop();

    push(lookup(_error_));                  // push the error state

    while (true)
    {
            // if on entry here token is already EOF then we've been here 
            // probably before: _error_ accepts EOF, but the state using
            // error nevertheless doesn't. In that case parsing terminates 
        if (d_token == _EOF_)
        {
            throw DEFAULT_RECOVERY_MODE;
        }
        try
        {
            nextToken();
            lookup(d_token);
            return d_token;
        }
        catch (...)
        {
        }
    }
}
catch (ErrorRecovery)       // This means: DEFAULT_RECOVERY_MODE
{
    ABORT();
    return 0;               // not reached. Inserted to prevent complaints
}                           // from the compiler

int Parser::parse()
try 
{
    push(0);                                // initial state

    while (true)
    {
        try
        {
            int action = lookup(d_token);   // d_state, token

            if (action > 0)                 // push a new state
            {
                push(action);
                d_token = _UNDETERMINED_;
            }
            else if (action < 0)
            {
                int saveToken = d_token;
                executeAction(-action);
                push(lookup(reduce(s_productionInfo[-action]))); 
                d_token = saveToken;
            }
            else 
                ACCEPT();
        }
        catch (ErrorRecovery)
        {
            d_token = errorRecovery();
        }
    }
}
catch (Return retValue)
{
    return retValue;
}

