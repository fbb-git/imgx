#include "parser.ih"

void Parser::checkCatListSize()
{
    unsigned nLanguages = d_language.size();

    if (d_langIdx > nLanguages)
        return;

    if (d_langIdx == nLanguages)
    {
        cout << "Line " << d_scanner.lineNr() << ": more categories " <<
                "than languages (= " << nLanguages << ") defined\n";
        d_semantic++;
        return;
    }

    unsigned firstCatSize = d_language[0].catSize();
    unsigned catSize = d_language[d_langIdx].catSize();

    if (firstCatSize  != catSize)
    {
        cout << "Line " << d_scanner.lineNr() << ": " <<
                catSize << " categories defined for language " <<
                                                    (d_langIdx + 1) << ",\n"
                "but " << firstCatSize <<
                        " categories were defined for the 1st language.\n";
        d_semantic++;
    }

    d_langIdx++;
}
