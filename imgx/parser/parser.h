#ifndef Parser_h_included
#define Parser_h_included

#include <iostream>

// The Parser holds all information about the images. To obtain a list of
// images indices given a certain category, use the Queue vector. The Queue
// vector returns a category index number whose name can be queried from the
// language vector, using the current language index. To obtain de name of
// the file containing an image, use fileContaining()

// the parser's base class header and possibly the namespace:
#include "parserbase.h"

#include <vector>
#include <string>
#include <bobcat/argconfig>
#include "../scanner/scanner.h"
#include "../image/image.h"
#include "../language/language.h"
#include "../queue/queue.h"

#undef Parser
class Parser: public ParserBase
{
    private:
        typedef std::vector<std::string> StringVector;
        
        FBB::ArgConfig                     &d_arg;
        Scanner                             d_scanner;
        std::vector<Language>               d_language;
        std::vector<Image>                  d_image;    // `image xxx' info
        std::vector<Queue>                  d_queue;
        unsigned                            d_semantic;
        unsigned                            d_langIdx;

    public:
        Parser();
        bool errors();
        std::vector<Image> const &image() const;
        int parse();
        Language const &lang(unsigned idx) const;
        std::vector<Language> const &languages() const;
        std::vector<Image> const &images() const;
        Image const &image(unsigned idx) const;
        std::vector<Queue> const &queues() const;
        Queue const &queue(unsigned idx) const;
        unsigned nImages() const;
        unsigned nQueues() const;

    private:
        static bool hasName(Image const &img, std::string const &name);

        void addImage(std::string const &name);
        void addCategory(std::string const &name);
        void checkCatCount();
        void checkCatListSize();
        void checkDescCount();
        void checkLastStepSize();

        void pushCatName(std::string const &name);
        void syntaxError();
        void error();

        int lex();

        void print_();
        void exceptionHandler(std::exception const &exc);

        void print();

        void executeAction_(int ruleNr);
        void errorRecovery_();
        int lookup(bool recovery);
        void nextToken_();
        void nextCycle_();
};

inline bool Parser::errors()
{
    return d_semantic || d_nErrors_;
}

inline std::vector<Image> const &Parser::image() const
{
    return d_image;
}

inline Language const &Parser::lang(unsigned idx) const
{
    return d_language[idx];
}

inline std::vector<Language> const &Parser::languages() const
{
    return d_language;
}

inline std::vector<Image> const & Parser::images() const
{
    return d_image;
}

inline Image const &Parser::image(unsigned idx) const
{
    return d_image[idx];
}

inline Queue const &Parser::queue(unsigned idx) const
{
    return d_queue[idx];
}

inline std::vector<Queue> const &Parser::queues() const
{
    return d_queue;
}

inline unsigned Parser::nImages() const
{
    return d_image.size();
}

inline unsigned Parser::nQueues() const
{
    return d_queue.size();
}

#endif
