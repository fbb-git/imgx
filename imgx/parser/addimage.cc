#include "parser.ih"

void Parser::addImage(string const &name)
{
    if
    (
        find_if(d_image.begin(), d_image.end(),
            [&](Image const &img)
            {
                return hasName(img, name);
            }
        ) !=
        d_image.end()
    )
    {
        cout << "Line " << d_scanner.lineNr() << ": Image `" << name <<
                "' multiply defined\n";
        d_semantic++;
        return;
    }
    d_image.push_back(Image(name, d_image.size()));
}




