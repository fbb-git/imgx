#include "parser.ih"

void Parser::syntaxError()
{
    cout << "[" << d_scanner.filename() << ": " << d_scanner.lineNr() << 
            "] syntax error at `" << d_scanner.matched() << "'" << endl;
}
