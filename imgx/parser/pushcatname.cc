#include "parser.ih"

void Parser::pushCatName(string const &name)
{
    if (d_langIdx < d_language.size())
        d_language[d_langIdx].pushCat(name);
}
