#include "parser.ih"

void Parser::checkLastStepSize()
{
    unsigned steps = d_language.back().stepSize();

    if (steps != 4)
    {
        cout << "Line " << d_scanner.lineNr() << ": language # " <<
                d_language.size() << " has " << steps <<
                                                " rather than  4 steps:\n" <<
                "Specify `first previous next last' for each language\n";
        d_semantic++;
    }
    d_langIdx = 0;
}
