#include "parser.ih"

void Parser::checkDescCount()
{
    unsigned count = d_image.back().size();

    if (count != d_language.size())
    {
        cout << "Line " << d_scanner.lineNr() << ": Image `" <<
                d_image.back().id() << "': requires " << d_language.size() <<
                " description(s), instead of " << count << endl;

        ++d_semantic;
    }
}
