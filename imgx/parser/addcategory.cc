#include "parser.ih"

void Parser::addCategory(std::string const &name)
{
    StringVector::const_iterator it =
                find(d_language[0].catBegin(), d_language[0].catEnd(), name);

    if (it == d_language[0].catEnd())
    {
        cout << "Line " << d_scanner.lineNr() << ": No category `" << name <<
                "' defined for language 1\n";
        d_semantic++;
        return;
    }

    unsigned idx = it - d_language[0].catBegin();

    if (idx >= d_queue.size())
        d_queue.resize(idx + 1);

    d_queue[idx].setCatIdx(idx);
    d_queue[idx].push_back(d_image.size() - 1);
}
