#include "parser.ih"

void Parser::checkCatCount()
{
    if (d_language.back().catSize() != d_language[0].catSize())
    {
        cout << "Line " << d_scanner.lineNr() << ": " <<
                " fewer categories defined than " << d_language.size() <<
                " (# of languages)\n";
        d_semantic++;
    }
}
