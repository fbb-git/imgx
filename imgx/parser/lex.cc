#include "parser.ih"

int Parser::lex()
try
{
    return d_scanner.lex();
}
catch (Scanner::Error err)
{
    cerr << "Include specification error in " << d_scanner.filename() <<
                        ", line " << d_scanner.lineNr() << endl;
    throw Exception() << "#include directive error";
}

