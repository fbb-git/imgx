#include "generator.ih"

string Generator::outStreamName(unsigned streamIdx)
{
    ostringstream name;
    name << outbase() << langIdx();

    if (streamIdx == static_cast<unsigned>(~0))
        name << ".html";
    else    
        name << streamIdx << ".php";

    return name.str();
}
