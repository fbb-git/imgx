#include "generator.ih"

//      table:
//
//      left col: image and         right col:  link to generic index
//                caption                       linktable

void Generator::showImage(Image const &image)
{
    out() <<
        "<table> <!-- image -->\n"
        "<tr>\n"
        "   <td align=\"center\">\n"
        "   <img src=\"" << imgPrefix() << image.id() << imgExt() << "\">\n"
        "   <p>\n" <<
        "   " << image.description(langIdx()) << "\n"
        "   </td>\n";

    linkTable(image.idx());

    out() <<
        "</tr>\n"
        "</table> <!-- image -->\n" <<
        "<p>\n";
}


