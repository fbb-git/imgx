#include "generator.ih"

void Generator::createOutStream(unsigned streamIdx)
{
    string name = outStreamName(streamIdx);

    cout << "Writing `" << name.c_str() << "'" << endl;
    out().open(name.c_str());

    out() <<                                    // the general table
        "<html>\n"
        "<head>\n"
        "   <style type=\"text/css\">\n"
        "   <!--\n"
        "       body { background: " << 
                        (d_background.length() ? d_background : d_bgcolor) << 
                                                                        ";\n"
        "              color: " << d_fgcolor << " }\n"
        "       img { image-orientation: none }\n"
        "   -->\n"
        "   </style>\n"
        "</head>\n"
        "<body>\n";
}

