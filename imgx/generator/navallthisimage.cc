#include "generator.ih"

void Generator::navAlThisImage(unsigned idx)
{
    unsigned last = nImages() - 1;

                                                // build the first href-table
                                                // which is always the
                                                // chronological link.
    string const &cat = lang().all();

    row
    (
        cat,
        href(cat, "1", 0),
        idx ? href(cat, to_string(idx), idx - 1)              : "-",
        idx < last ? href(cat, to_string(idx + 2), idx + 1)   : "-",
        href(cat, to_string(last + 1), last)
    );

    rowline();
}

