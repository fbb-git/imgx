#include "generator.ih"

void Generator::addExplanation()
{
    string basename;

    if (!ArgConfig::instance().option(&basename, 'e'))
        return;

    basename += '.' + to_string(langIdx());

    ifstream in(basename.c_str());

    if (!in)
        return;

    out() << "<p>" << in.rdbuf() << "\n";
}
