#include "generator.ih"

void Generator::htmlText(Image const &image) 
{
    copy(image.html(langIdx()).begin(), image.html(langIdx()).end(),
        ostream_iterator<string>(out(), "\n"));
}

