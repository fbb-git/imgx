#include "generator.ih"

void Generator::imageFiles()
{
    d_streamIndex = static_cast<unsigned>(-1);
                                    // stream #'s start at 0, but
                                    // createOutStream in genImage()
                                    // increments d_streamIndex when opening a
                                    // file  

    for(auto &img: images())
        image(img);

    closeOutStream();
}

