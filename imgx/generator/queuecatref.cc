#include "generator.ih"

void Generator::queueCatRef(Queue const &queue)
{
    if (!nQueues())
        return;

    if (queue.size() == 0)
    {
        d_warnings << "[Warning] language " << langIdx() << ": category `" << 
                        lang().catName(d_lastCatIndex) << "' not used\n";
        return;
    }

    string const &cat = lang().catName(queue.catIdx());

    row(
        cat,
        href(cat, "1", queue[0]),
        href(cat, to_string(queue.size()), queue.back()),
        "", ""
    );
}
