#include "generator.ih"

Generator::Generator(Parser const &parser)
:
    WriteBase(parser),
    d_php(*this)
{
    ArgConfig &arg = ArgConfig::instance();

    if (arg.option(&d_background, 'B'))
        d_background = " url(" + d_background + ");";
    if (!arg.option(&d_bgcolor, 'b'))
        d_bgcolor = " white";
    if (!arg.option(&d_fgcolor, 'f'))
        d_fgcolor = " black";


}
