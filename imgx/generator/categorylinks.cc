#include "generator.ih"

void Generator::categoryLinks(unsigned idx)
{
    for (unsigned qidx = 0; qidx < nQueues(); ++qidx)
    {
        Queue const &qu = queue(qidx);

        //cerr << "Image " << idx << " in category " << qidx << " (" <<
        //lang().catName(qu.catIdx()) << ")? ";

        if (!qu.find(idx))
        {
            //cerr << "No: "
            //        "q-img idx = " << qu.currentImageIndex() << endl;
            continue;
        }

        // cerr << "YES: " << 
        //          "q-img idx = " << qu.currentImageIndex() << endl;

        out() << "<tr>\n";

        string const &cat = lang().catName(qu.catIdx());
        col(cat);

        col(!idx ? "1" : href(cat, "1", qu[0]));              // first

        col(!qu.hasPrevious() ? "-" :
                href(cat, 
                     to_string(qu.previousNr()), qu.previousImageIndex()));

        col(!qu.hasNext() ? "-"   :
                href(cat, to_string(qu.nextNr()), qu.nextImageIndex()));

        col(qu.atLast() ? string(to_string(qu.lastNr())) :
                href(cat, to_string(qu.lastNr()), qu.lastImageIndex()));

        out() << "</tr>\n";
    }
}
