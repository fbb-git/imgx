#include "generator.ih"

void Generator::navAllImages()
{
                                                // build the first href-table
                                                // which is always the
                                                // chronological link.
    string const &cat = lang().all();
    row(cat,   href(cat, "1", 0),
               href(cat, to_string(nImages()), nImages() - 1),
               "", "");
    rowline();
}

