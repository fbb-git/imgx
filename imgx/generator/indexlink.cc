#include "generator.ih"

void Generator::indexLink()         // link to the generic index file
{
    out() <<
        "<a href=\"" << outbase() << langIdx() << ".html\">" <<
            lang().index() << "</a>\n"
        "<p>\n";
}

