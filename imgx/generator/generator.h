#ifndef _INCLUDED_GENERATOR_
#define _INCLUDED_GENERATOR_

#include <string>
#include <sstream>

#include "../writebase/writebase.h"
#include "../php/php.h"

class Parser;
class Generator: private WriteBase
{
    Php d_php;
    unsigned d_lastCatIndex;
    unsigned d_streamIndex;
    std::string                         d_background;
    std::string                         d_bgcolor;
    std::string                         d_fgcolor;

    std::ostringstream                  d_warnings;

    public:
        Generator(Parser const &parser);

        void generate();

        ~Generator();

    private:
        void language(Language const &language);

        void indexFile();
        void createOutStream(unsigned idx = ~0);
        std::string outStreamName(unsigned idx = ~0);

        void navAllImages();            // nav table for all images
        std::string href(std::string const &cat, 
                         std::string const &link, unsigned imgIdx);
        std::string fileContaining(unsigned imageIdx);
        void navAllCategories();        // nav table for all categories

        void queueCatRef(Queue const &queue);

        void addExplanation();

        void closeOutStream();

        void imageFiles();
        void image(Image const &image);
        void imageHeader(Image const &image);
        void htmlText(Image const &image);
        void showImage(Image const &image);
        void linkTable(unsigned idx);
        void indexLink();            // link to the generic index file
        void navAlThisImage(unsigned idx);  // all images nav table entry for 
                                            // this image

        void categoryLinks(unsigned idx);

};

        
#endif
