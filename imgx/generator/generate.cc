#include "generator.ih"

void Generator::generate()
{
    for (auto &lang: languages())
        language(lang);
}
