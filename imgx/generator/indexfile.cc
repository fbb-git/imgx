#include "generator.ih"

void Generator::indexFile()
{
    createOutStream(~0);                        // create html-index page's
                                                // head 
    startNavTable(INDEXFILE);                   // write the navigation labels

    navAllImages();                             // links to all images

    navAllCategories();                         // links for all categories

    endNavTable();                              // end the navigation table

    addExplanation();                           // add the explanation file

    closeOutStream();
}



