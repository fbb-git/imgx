#include "generator.ih"

string Generator::href(string const &cat, string const &link, 
                       unsigned imageIdx)
{
    return
        "<a href=\"" +
            fileContaining(imageIdx) + "?cat=" + cat + "#i" + 
            static_cast<string>(to_string(imageIdx)) + 
        "\">" + link + "</a>";
}
