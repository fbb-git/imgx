#include "generator.ih"

void Generator::navAllCategories()
{
    d_lastCatIndex = 0;

    for (auto &queue: queues())
    {
        queueCatRef(queue);
        ++d_lastCatIndex;
    }
}

