#include "generator.ih"

void Generator::image(Image const &image)
{
    unsigned idx = image.idx();

    if (idx % imagesPerFile() == 0)
    {
        closeOutStream();
        createOutStream(++d_streamIndex);
        d_php.catLinkArray(idx);                  // create the catlink-arrays
                                                // and function for this file.
    }

    imageHeader(image);                      // label <h2>, catlinkcall

    htmlText(image);    
    showImage(image);                           // image itself + linktable

}





