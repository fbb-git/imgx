#include "generator.ih"

void Generator::closeOutStream()
{
    if (!out().is_open())
        return;

    out() <<
        "</body>\n"
        "</html>\n";

    out().close();
}
