#include "generator.ih"

void Generator::imageHeader(Image const &image)
{
    unsigned idx = image.idx();
    out() <<
        "<a name=\"i" << idx << "\"></a><br>\n"
        "<h2>" << image.description(langIdx()) << "</h2>\n"
        "<?php catLink(" << idx << ");?>\n";
}

