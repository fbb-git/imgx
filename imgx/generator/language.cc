#include "generator.ih"

void Generator::language(Language const &language)
{
    indexFile();                             // generate the html-index
                                             // page
    imageFiles();                            // generate the php image 

    incLangIdx();                            // maybe do it again for the
                                             // next language
}



