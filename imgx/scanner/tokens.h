#ifndef INCLUDED_TOKENS_
#define INCLUDED_TOKENS_

struct Tokens
{
    // Symbolic tokens:
    enum Tokens_
    {
        CAT = 257,
        IDENT,
        LINKS,
        STRING,
        IMAGE,
        HTML,
        LINE,
    };

};

#endif
